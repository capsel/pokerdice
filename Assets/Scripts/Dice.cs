﻿/*
 *\File Dice.cs 
 *\Authors Monika Drozdowska, Konrad Babiarz, Jakub Nowak
 *\Date 22.06.2018r
*/

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Klasa Kość
/// </summary>
public class Dice : MonoBehaviour
{
    #region Deklaracje
    public event System.Action OnClick;

    public Image myImage;
    public Color holdColor, lockedColor;
    public Sprite[] faceSprites;
    public int faceIndex = 0;
    public Button button;

    public HoldState hold;
    #endregion

    #region Public Methods

    /// <summary>
    /// Metoda obsługująca kliknięcie w kość
    /// </summary>
    public void Dice_Click()
    {
        if (hold == HoldState.None)
            hold = HoldState.Held;
        else if (hold == HoldState.Held)
            hold = HoldState.None;

        switch (hold)
        {
            case HoldState.None:
                myImage.color = Color.white;
                break;
            case HoldState.Held:
                myImage.color = holdColor;
                break;
            case HoldState.Locked:
                myImage.color = lockedColor;
                break;
        }

        if (OnClick != null)
            OnClick();
    }

    /// <summary>
    /// Metoda ustawiania losowych zdjęć na kościach
    /// </summary>
    public void ShowRandomFace()
    {
        faceIndex = Random.Range(1, 6);
        myImage.sprite = faceSprites[faceIndex];
    }


    // for testing
    /// <summary>
    /// Metoda dla testów pozwalająca ustawić wskazaną wartość
    /// </summary>
    /// <param name="p">Pomocnicza ustawiająca zdjęcie dla danej wartości kości</param>
    public void ShowNumber(int p)
    {
        faceIndex = p;
        myImage.sprite = faceSprites[faceIndex];
    }
    /// <summary>
    /// Metoda do blokowania możliwości zmiany kości
    /// </summary>
    public void Lock()
    {
        hold = HoldState.Locked;
        myImage.color = lockedColor;
    }
    #endregion
}

/// <summary>
/// Tryb enumeryczny
/// </summary>
public enum HoldState 
{
    None,
    Held,
    Locked
}
