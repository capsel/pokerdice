﻿/*
 *\File SceneController.cs 
 *\Authors Monika Drozdowska, Konrad Babiarz, Jakub Nowak
 *\Date 22.06.2018r
*/

using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Klasa SceneController do obsługi zdarzeń
/// </summary>
public class SceneController : MonoBehaviour
{
    #region Deklaracje
    // Zadelkarowanie zmiennych Kosci, Texty, Button i pomocnicze
    public Dice[] dices;
    public Text handText, currentScoreText, scoreInBankText;
    public Button rollDiceButton;

    int scoreInBank = 0;
    // the score of the previous turn
    int previousScore = 0;

    // score of the current held dice
    int currentScore = 0;
    //string currentScoreDescription = "";
    int currentScorePoints = 0;
    int[] repeats = { 0, 0, 0, 0, 0, 0 };
    #endregion

    #region Inicjalizacja

    /// <summary>
    /// Metoda do startu aplikacji Unity ( LOAD ) 
    /// </summary>
    void Start()
    {
        for (int i = 0; i < dices.Length; i++)
        {
            dices[i].button.interactable = false; // in the beginning, player must not be able to hold any dice
            dices[i].OnClick += Dice_OnClick;
        }
    }
    #endregion

    #region Public methods

    /// <summary>
    /// In future - banks the score click.
    /// Now, do nothing.
    /// </summary>
    public void BankScore_Click()
    {
        //Do nothing
    }
    /// <summary>
    /// Metoda wywoływana w momencie kliknięcia w kość
    /// </summary>
    void Dice_OnClick()
    {
        List<int> heldDice = new List<int>();
        for (int i = 0; i < dices.Length; i++)
        {
            if (dices[i].hold != HoldState.None)
                heldDice.Add(dices[i].faceIndex);

            repeats[i] = dices[i].faceIndex;
        }

        rollDiceButton.interactable = true;
    }


    /// <summary>
    /// Metoda zwracająca numeryczną wartość układu kości
    /// Funkcja pomocnicza
    /// </summary>
    /// <returns>Zwraca wynik numeryczny</returns>
    /// <param name="repeats">Pobiera tablice wartosci kości gracza</param>
    public int getScore(int[] repeats)
    {

        //POKER
        if (repeats[0] == 5 || repeats[1] == 5 || repeats[2] == 5 || repeats[3] == 5 || repeats[4] == 5 || repeats[5] == 5)
        {
            return 1000;
        }

        //KARETA
        if (repeats[0] == 4 || repeats[1] == 4 || repeats[2] == 4 || repeats[3] == 4 || repeats[4] == 4 || repeats[5] == 4) // kareta
        {
            return 800;
        }
        //return "KARETA - cztery kości o tej samej liczbie oczek";

        //Sprawdzenie liczby trójek i par, żeby ustalić czy występuje full
        int trios = 0;
        int pairs = 0;
        for (int i = 0; i < repeats.Length; i++)
        {
            if (repeats[i] == 3) { trios++; }
            if (repeats[i] == 2) { pairs++; }
        }

        //FULL
        if (trios == 1 && pairs == 1)
        {
            return 600;//"Full - jedna para i trójka";
        }

        //STRIT
        //duży strit nie zawiera 1, a zawiera 6, więc sprawdzamy liczbę powtórzeń 6 (ma index 5).
        if (repeats[1] == 1 && repeats[2] == 1 && repeats[3] == 1 && repeats[4] == 1 && repeats[5] == 1)
        {
            return 500;//"Duży Strit -kości pokazujące wartości od 2 do 6, po kolei";
        }
        else if (repeats[0] == 1 && repeats[1] == 1 && repeats[2] == 1 && repeats[3] == 1 && repeats[4] == 1)
        {
            return 400;//"Mały Strit -kości pokazujące wartości od 1 do 5, po kolei";
        }

        //TRÓJKA 
        if (trios == 1)
        {
            return 300;//"Trójka - trzy kości o tej samej liczbie oczek";
        }
        //DWIE PARY 
        if (pairs == 2)
        {
            return 200;//"Dwie Pary - dwie pary kości, o tej samej liczbie oczek";
        }
        if (pairs > 0)
        {
            return 100;//"Para - dwie kości o tej samej liczbie oczek";
        }
        return 0;// "Przypadek - pięć nie tworzących żadnego układu oczek";

    }

    /// <summary>
    /// Metoda wywoływana przez przycisk losowania
    /// Ustawianie losowych wartości kości
    /// </summary>
    public void RollDice_Click()
    {
        repeats[0] = 0;
        repeats[1] = 0;
        repeats[2] = 0;
        repeats[3] = 0;
        repeats[4] = 0;
        repeats[5] = 0;

        //Dla każdej nie zaznaczonej kości ustawia losową wartość
        for (int i = 0; i < dices.Length; i++)
        {

            if (dices[i].hold == HoldState.None)
            {
                dices[i].ShowRandomFace();

            }
            else
            {
                dices[i].Lock();
            }

            int helper = dices[i].faceIndex;
            repeats[helper]++;
        }


        //Wywołanie metody pomocniczej zwracającej wartość układu kości
        int goal = getScore(repeats);

        string value = goal.ToString();
        string description = detectResult(goal).ToString();
        currentScoreText.text = value;
        currentScore = currentScorePoints;
        handText.text = description;
        rollDiceButton.interactable = false;

        for (int i = 0; i < dices.Length; i++)
        {
            dices[i].button.interactable = true; // player must be able to hold dice
        }
    }

    /// <summary>
    /// Metoda zwracająca tekstową wartość układu kości
    /// </summary>
    /// <returns>Wartość tekstowa</returns>
    /// <param name="result">Zmienna pomocnicza odwołująca się do trybu enum</param>
    public Goals detectResult(int result)
    {
        switch (result)
        {
            case 1000: return Goals.Poker;
            case 800: return Goals.Kareta;
            case 600: return Goals.Full;
            case 500: return Goals.Duzy_strit;
            case 400: return Goals.Maly_strit;
            case 300: return Goals.Trojka;
            case 200: return Goals.Dwie_pary;
            case 100: return Goals.Para;
            default: return Goals.Nic;
        }
    }
#endregion
}


// Tryb enumeryczny do wyswietlania wyników
public enum Goals
{
    [Description("Poker")] Poker,
    [Description("Kareta")] Kareta,
    [Description("Full")] Full,
    [Description("Duży Strit")] Duzy_strit,
    [Description("Mały Strit")] Maly_strit,
    [Description("Trójka")] Trojka,
    [Description("Dwie pary")] Dwie_pary,
    [Description("Para")] Para,
    [Description("Nic śmiesznego")] Nic,
}