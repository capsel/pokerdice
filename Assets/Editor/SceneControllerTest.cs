﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

/// <summary>
/// Scene controller test.
/// </summary>
public class SceneControllerTest
{

    #region Deklaracje
    SceneController sceneController = new SceneController();
    public Dice[] dices;
    string description = "";
    int[] repeats = { 0, 0, 0, 0, 0, 0 };
    #endregion

    #region Public methods
    /// <summary>
    /// Randomizer exclude the specified i.
    /// </summary>
    /// <returns>The integer.</returns>
    /// <param name="i">The excluded number.</param>
    public int Randomizer(int i)
    {
        int number = UnityEngine.Random.Range(1, 6);
        if (number == i)
        {
            return Randomizer(i);
        }
        return number;
    }

    /// <summary>
    /// Test układu kości poker - 5 jednakowych kości
    /// </summary>
    [Test]
    public void PokerTest()
    {
        //arrange
        for (int i = 0; i < 6; i++)
        {
            repeats[i] = 5;
            //act
            var score = sceneController.getScore(repeats);
            description = sceneController.detectResult(score).ToString();
            //assert
            Assert.AreEqual(1000, score);
            Assert.AreEqual(Goals.Poker.ToString(), description);
            repeats[i] = 0;
        }
    }

    /// <summary>
    /// Test układu kareta - 4 jednakowe kości i jedna losowa.
    /// </summary>
    [Test]
    public void KaretaTest()
    {
        //arrange
        for (int i = 0; i < 6; i++)
        {
            repeats[i] = 4;
            int j = Randomizer(i);
            repeats[j] = 1;
            //act
            var score = sceneController.getScore(repeats);
            description = sceneController.detectResult(score).ToString();
            //assert
            Assert.AreEqual(800, score);
            Assert.AreEqual(Goals.Kareta.ToString(), description);
            repeats[i] = 0;
            repeats[j] = 0;

        }
    }

    /// <summary>
    /// Test układu full - trójka i para
    /// </summary>
    [Test]
    public void FullTest()
    {
        //arrange
        for (int i = 0; i < 6; i++)
        {
            repeats[i] = 3;
            int j = Randomizer(i);
            repeats[j] = 2;
            //act
            var score = sceneController.getScore(repeats);
            description = sceneController.detectResult(score).ToString();
            //assert
            Assert.AreEqual(600, score);
            Assert.AreEqual(Goals.Full.ToString(), description);
            repeats[i] = 0;
            repeats[j] = 0;

        }
    }

    /// <summary>
    /// Test układu duży strit - kolejne kości od 2 do 6.
    /// </summary>
    [Test]
    public void Duzy_stritTest()
    {

        int i = 0;
        repeats[i] = 0;
        //arrange
        for (i = 1; i < 6; i++)
        {
            repeats[i] = 1;
        }

        //act
        var score = sceneController.getScore(repeats);
        description = sceneController.detectResult(score).ToString();
        //assert
        Assert.AreEqual(500, score);
        Assert.AreEqual(Goals.Duzy_strit.ToString(), description);
    }

    /// <summary>
    /// Test układu mały strit - kości kolejno od 1 do 5.
    /// </summary>
    [Test]
    public void Maly_stritTest()
    {
        int i = 5;
        repeats[i] = 0;
        //arrange
        for (i = 4; i >= 0; i--)
        {
            repeats[i] = 1;
        }

        //act
        var score = sceneController.getScore(repeats);
        description = sceneController.detectResult(score).ToString();
        //assert
        Assert.AreEqual(400, score);
        Assert.AreEqual(Goals.Maly_strit.ToString(), description);
    }

    /// <summary>
    /// Test układu trójka - 3 jednakowe i 2 losowe kości.
    /// </summary>
    [Test]
    public void TrojkaTest()
    {
        //arrange
        for (int i = 0; i < 6; i++)
        {
            repeats[i] = 3;
            int j = Randomizer(i);
            repeats[j] = 1;
            int k = 0;

            while (i == k || j == k)
            {
                k = Randomizer(i);
            }
            repeats[k] = 1;
            //act
            var score = sceneController.getScore(repeats);
            description = sceneController.detectResult(score).ToString();
            //assert
            Assert.AreEqual(300, score);
            Assert.AreEqual(Goals.Trojka.ToString(), description);
            repeats[i] = 0;
            repeats[j] = 0;
            repeats[k] = 0;

        }
    }

    /// <summary>
    /// Test układu dwie pary - dwie pary o różnej liczbie oczek na kostkach, oraz jedna dodatkowa losowa kość.
    /// </summary>
    [Test]
    public void DwieParyTest()
    {
        for (int i = 0; i < 6; i++)
        {
            repeats[i] = 2;
            int j = Randomizer(i);
            repeats[j] = 2;
            int k = 0;
            while (i == k || j == k)
            {
                k = Randomizer(i);
            }
            repeats[k] = 1;
            //act
            var score = sceneController.getScore(repeats);
            description = sceneController.detectResult(score).ToString();
            //assert
            Assert.AreEqual(200, score);
            Assert.AreEqual(Goals.Dwie_pary.ToString(), description);
            repeats[i] = 0;
            repeats[j] = 0;
            repeats[k] = 0;

        }
    }

    /// <summary>
    /// Test układu para - dwie kości o tej samej liczbie oczek oraz trzy różne kości.
    /// </summary>
    [Test]
    public void ParaTest()
    {
        int j = 0;
        int k = 0;
        int l = 0;
        //arrange
        for (int i = 0; i < 6; i++)
        {
            repeats[i] = 2;
            j = Randomizer(i);
            repeats[j] = 1;
            //act
            while (i == k || j == k)
            {
                k = Randomizer(i);
            }
            repeats[k] = 1;
            while (l == i || l == j || l == k)
            {
                l = Randomizer(i);
            }
            repeats[l] = 1;
            var score = sceneController.getScore(repeats);
            description = sceneController.detectResult(score).ToString();
            //assert
            Assert.AreEqual(100, score);
            Assert.AreEqual(Goals.Para.ToString(), description);
            repeats[i] = 0;
            repeats[j] = 0;
            repeats[k] = 0;
            repeats[l] = 0;
        }
    }

    /// <summary>
    /// Test układu nic - każda kość o innej wartości, dodatkowo z wykluczeniem obu stritów (nie kolejne liczby).
    /// </summary>
    [Test]
    public void NicTest()
    {
        //arrange
        for (int i = 0; i < 6; i++)
        {
            repeats[i] = 1;
        }
        //W związku z tym, że chcemy każdą liczbę inną i nie chcemy strita, losujemy j z zakresu 1,4 (odpowiednik oczek 2-5, czyli z środka zakresu) i zerujemy, bo mieliśmy o jedno powtórzenie za dużo z powyższej pętli
        int j = UnityEngine.Random.Range(1, 4);
        repeats[j] = 0;
        var score = sceneController.getScore(repeats);
        description = sceneController.detectResult(score).ToString();
        //assert
        Assert.AreEqual(0, score);
        Assert.AreEqual(Goals.Nic.ToString(), description);
        for (int i = 0; i < 6; i++)
        {
            repeats[i] = 0;
        }
    }
    #endregion
}
