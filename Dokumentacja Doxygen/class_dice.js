var class_dice =
[
    [ "Dice_Click", "class_dice.html#aa0b50db9f9b5c000e08f5a2a702be157", null ],
    [ "Lock", "class_dice.html#a82e9c575010b2ae8a2bb84411bda27a4", null ],
    [ "ShowNumber", "class_dice.html#a58377bc8fea1f2fe48f6986612d8a587", null ],
    [ "ShowRandomFace", "class_dice.html#a0466e031e41060a9a71cbdbfa85a54cc", null ],
    [ "button", "class_dice.html#ab9adb22f20c770628a926679e5f1fedb", null ],
    [ "faceIndex", "class_dice.html#ab99d321fba3ecf68edce57495148ae1b", null ],
    [ "faceSprites", "class_dice.html#ab5e4411382ed836586cac0c467a994e0", null ],
    [ "hold", "class_dice.html#afe9591e1b772a89c6e380ede3b0ff690", null ],
    [ "holdColor", "class_dice.html#a885c313a42ff492d63c47e989b64c6d0", null ],
    [ "myImage", "class_dice.html#adf9baa5a4630a9f41c960b719fd604f0", null ],
    [ "OnClick", "class_dice.html#ab788d5ad44f5737336ea99143acdff15", null ]
];